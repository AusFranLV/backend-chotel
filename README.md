# Backend CHotel

## Preparación del backend
1. Clonar este repositorio.
2. Ejecutar `npm install` dentro del directorio clonado para instalar todas las dependencias.
3. En el directorio raíz del proyecto, editar el archivo `.env` segun la configuración de la db

### Configuración

Fijar o cambiar estos valores en el archivo `.env`

| Nombre de la variable | Descripción | Valor por defecto |
| ------------- | ----------- | ------------- |
| DB_HOST | El host o la dirección IP del servidor de base de datos. | `127.0.0.1` |
| DB_NAME | El nombre de la base de datos a utilizar. **Requerido**. | |
| DB_USER | El nombre de usuario utilizado para acceder a la base de datos. **Requerido**. | |
| DB_PASS | La contraseña utilizada para acceder a la base de datos. **Requerido**. | |

### Configuraciones de base de datos
Para configurar la base de datos:
* Se debe ejecutar `npm run dev:db:setup` para correr las migraciones y seeders por defecto (configurar las tablas).

### Iniciar backend en modo desarollo
* En el directorio raíz del proyecto ejecutar `npm start` para iniciar el servidor en modo de desarrollo.

### Iniciar backend en modo producción con PM2
* En el directorio raíz del proyecto ejecutar `npm run build` para precompilar los archivos. 
