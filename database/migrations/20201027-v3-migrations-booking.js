module.exports = {
  up(queryInterface, Sequelize) {
    return queryInterface.createTable('booking', {
      id: {
        type: Sequelize.BIGINT(20),
        autoIncrement: true,
        primaryKey: true,
      },
      roomId:  {
        type: Sequelize.BIGINT(20),
        field: 'room_id',
        references: {
          model: 'room',
          key: 'id',
        },
      },
      date_start: Sequelize.STRING(20),
      date_end: Sequelize.STRING(20),
      status: {
        type: Sequelize.INTEGER(1),
        defaultValue: 0,
      }
    });
  },

  down(queryInterface) {
    queryInterface.dropTable('booking');
  },
};