module.exports = {
  up(queryInterface, Sequelize) {
    return queryInterface.createTable('room', {
      id: {
        type: Sequelize.BIGINT(20),
        autoIncrement: true,
        primaryKey: true,
      },
      type_room_id:  {
        type: Sequelize.BIGINT(20),
        references: {
          model: 'type_room',
          key: 'id',
        },
      },
      name: Sequelize.STRING(50),
      status: {
        type: Sequelize.INTEGER(1),
        defaultValue: 0,
      }
    });
  },

  down(queryInterface) {
    queryInterface.dropTable('room');
  },
};