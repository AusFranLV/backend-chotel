module.exports = {
  up(queryInterface, Sequelize) {
    return queryInterface.createTable('room_type', {
      id: {
        type: Sequelize.BIGINT(20),
        autoIncrement: true,
        primaryKey: true,
      },
      name: Sequelize.STRING(50),
    });
  },

  down(queryInterface) {
    queryInterface.dropTable('room_type');
  },
};