module.exports = {
  up(queryInterface) {
    return queryInterface.bulkInsert('room_type', [
      { id: 1, name: 'Familiar' },
      { id: 2, name: 'Double' },
      { id: 3, name: 'Single' },
    ]);
  },

  down(queryInterface) {
    return queryInterface.bulkDelete('room_type', null, {});
  },
};
