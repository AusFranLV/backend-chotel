module.exports = {
  up(queryInterface) {
    return queryInterface.bulkInsert('room', [
      { name: 'Familiar Room 1', type_room_id: 1 },
      { name: 'Familiar Room 2', type_room_id: 1 },
      { name: 'Familiar Room 3', type_room_id: 1 },
      { name: 'Familiar Room 4', type_room_id: 1 },
      { name: 'Familiar Room 5', type_room_id: 1 },
      { name: 'Double Room 1', type_room_id: 2 },
      { name: 'Double Room 2', type_room_id: 2 },
      { name: 'Double Room 3', type_room_id: 2 },
      { name: 'Double Room 4', type_room_id: 2 },
      { name: 'Double Room 5', type_room_id: 2 },
      { name: 'Single Room 1', type_room_id: 3 },
      { name: 'Single Room 2', type_room_id: 3 },
      { name: 'Single Room 3', type_room_id: 3 },
      { name: 'Single Room 4', type_room_id: 3 },
      { name: 'Single Room 5', type_room_id: 3 },
    ]);
  },

  down(queryInterface) {
    return queryInterface.bulkDelete('room', null, {});
  },
};