const http = require('http');
const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const { generateDays, validateParamns } = require("./utils");
const { getAllRooms, getAllBooking, getBookingByRoomAvailability, addBooking, updateBooking, deleteBooking } = require("./integrations");

const PORT = 3000;
const app = express();

app.use(cors());

app.get('/api/rooms', async (req, res) => {
  return getAllRooms().then((rooms) => {
    res.json({
      error: false,
      statusCode: 200,
      mensaje: 'All rooms',
      response: rooms
    });
  });
});

app.get('/api/room/availability',  async (req, res) => {
  return getAllBooking().then((rooms) => {
    res.json({
      error: false,
      statusCode: 200,
      mensaje: 'All bookings',
      response: rooms
    });
  });
});

app.get('/api/room/availability/:type/:checkin/:checkout',  async (req, res) => {
  	const { type, checkin, checkout } = req.params;
    const validate = validateParamns(type, checkin, checkout);
    
    if (validate) {
      res.status(400).json({ 
        error: true,
        statusCode: 400,
        mensaje: validate
      });
      return
    }

    return getBookingByRoomAvailability({ type, checkin, checkout })
      .then(room => {
        if (!room) {
          res.json({
            error: false,
            statusCode: 200,
            mensaje: ' Room  availability'
          });
          return;
        }
        res.json({
          error: false,
          statusCode: 400,
          mensaje: ' Room no availability'
        });
        return
    }).catch(err => console.log(err))
});

app.post('/api/room/availability/:type/:checkin/:checkout',  async (req, res) => {
  const { type, checkin, checkout } = req.params;
  const validate = validateParamns(type, checkin, checkout)
    
  if (validate) {
    res.status(400).json({ 
      error: true,
      statusCode: 400,
      mensaje: validate
    });
    return
  }

  return addBooking({ type, checkin, checkout }).then(room => {
    if (room) {
      res.json({
        error: false,
        statusCode: 200,
        mensaje: 'Room Booking',
        response: room,
      });
      return;
    }
  }).catch(err => {
    res.json({
      error: false,
      statusCode: 400,
      mensaje: err.message,
    });
  })
});

app.put('/api/room/availability/:id/:type/:checkin/:checkout',  async (req, res) => {
  const { id, type, checkin, checkout } = req.params;
  const validate = validateParamns(type, checkin, checkout);
  
  if (validate) {
    res.status(400).json({ 
      error: true,
      statusCode: 400,
      mensaje: validate
    });
    return
  }

  
  return updateBooking({ id, type, checkin, checkout }).then(room => {
    if (room) {
      res.json({
        error: false,
        statusCode: 200,
        mensaje: ' Booking updated',
        response: room,
      });
      return;
    }
  }).catch(err => {
    res.json({
      error: false,
      statusCode: 400,
      mensaje: err.message,
    });
  });
});

app.delete('/api/room/availability/:id',  async (req, res) => {
  const { id } = req.params;

  return deleteBooking(id).then(room => {
    res.json({
      error: false,
      statusCode: 200,
      mensaje: ' Booking deleted',
      response: id
    });
  })
});

app.use(async (req, res, next) => {
  respuesta = {
    error: true, 
    statusCode: 404, 
    mensaje: 'URL Not found'
  };
 res.status(404).send(respuesta);
});

const server = http.createServer(app);

server.listen(PORT, () => {
  console.log(`API Server is now running on http://localhost:${PORT}`);
});