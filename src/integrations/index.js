const Promise = require('bluebird');
const db = require('../models');
const { Op } = require("sequelize");
const { generateDays } = require("../utils");

const getBookingByRoom = (b) => {
  return db.Room.findOne({
    where: { id: b.roomId }
  }).then(room => ({ id: b.id, title: room.name, allDay: true, description: room.id, start: b.dateStart, end: b.dateEnd }))
}

const getAllRooms = () => {
	return db.Room.findAll({
	    where: { status: 0 }
  	});
}

const getAllBooking = () => {
	return db.Booking.findAll({
    	where: { status: 0 }
  	}).then(bookings => Promise.map(bookings, b => getBookingByRoom(b)));
}

const getBookingByRoomAvailability = ({ type, checkin, checkout }) => {
    const days = generateDays(checkin, checkout);
	return db.Booking.findOne({ 
      	where: {
  			roomId: type,
        	[Op.or]: [
          		{ dateStart: days },
          		{ dateEnd: days }
        	]
    	},
    	raw: true
    });
}

const addBooking = ({ type, checkin, checkout }) => {
    const days = generateDays(checkin, checkout);
	return db.Booking.findOne({ 
    	where: {
        	roomId: type,
        	[Op.or]: [
          		{ dateStart: days },
          		{ dateEnd: days }
        	]
      	},
      	raw: true
    }).then(room => {
      	if (room) {
        	throw new Error('Room no availability');
        }
      	return db.Booking.create({ roomId: type, dateStart: checkin, dateEnd:checkout })
      		.then(booking => getBookingByRoom(booking))
    })
}

const updateBooking = ({id, type, checkin, checkout }) => {
    const days = generateDays(checkin, checkout);
    return db.Booking.findOne({ 
      	where: {
        	roomId: type,
        	id: { [Op.ne]: id },
        	[Op.or]: [
          		{ dateStart: days },
          		{ dateEnd: days }
        	]
      	},
      	raw: true
    }).then(bg => {
      	if (bg) {
        	throw new Error('Room no availability');
        }
      	return db.Booking.update({ roomId: type, dateStart: checkin, dateEnd:checkout }, { where: { id }
      	}).then(() => db.Booking.findOne({ where: { id} }).then(booking => getBookingByRoom(booking)))
  	});
}

const deleteBooking = (id) => {
	return db.Booking.destroy({
      where: { id }
    })
}

module.exports = {
  	getAllRooms,
  	getAllBooking,
  	getBookingByRoomAvailability,
  	addBooking,
  	updateBooking,
  	deleteBooking,
}