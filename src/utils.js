const moment = require('moment-timezone');

const validateParamns = (type, checkin, checkout) => {
    if (!type && !checkin && !checkout) {
      return ' fields type, date start and date end required';
    }

    if (!type) {
      return ' field type required';
    }

    if (!checkin && !checkout) {
      return ' fields date start and date end required';
    }

    if (!checkin) {
      return ' fields date start required';
    }

    if (!checkout) {
      return ' fields date end required';
    }

    if (checkin > checkout) {
      return " fields date start can't greater than date end";
    }

    if (isNaN(parseInt(type))) {
      return ' field type required';
    }

    return false;
}

const generateDays = (checkin, checkout) => {
  let days = [];
  let dayStart =  moment(checkin).format('YYYY-MM-DD');
  let dayEnd = moment(checkout).format('YYYY-MM-DD');
  let i = 0;
  do {
    days.push(dayStart);
    dayStart = moment(dayStart).add(1,'days').format('YYYY-MM-DD')
  } while (dayStart <=  dayEnd);

  return days;
}

module.exports = {
  generateDays, validateParamns
}
