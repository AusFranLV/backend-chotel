module.exports = (sequelize, DataTypes) => {
  const Booking = sequelize.define('Booking', {
    id: {
      type: DataTypes.BIGINT(20),
      autoIncrement: true,
      primaryKey: true,
    },
    roomId:  {
      type: DataTypes.BIGINT(20),
      field: 'room_id',
    },
    dateStart:{
      type: DataTypes.STRING(20),
      primaryKey: true,
      field: 'date_start',
    },
    dateEnd:{
      type: DataTypes.STRING(20),
      primaryKey: true,
      field: 'date_end',
    },
    status: {
      type: DataTypes.INTEGER(1),
      defaultValue: 0,
    }
  }, {
    tableName: 'booking',
    freezeTableName: true,
    timestamps: false,
    underscored: true,
  });

  return Booking;
};
