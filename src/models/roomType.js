module.exports = (sequelize, DataTypes) => {
  const RoomType = sequelize.define('RoomType', {
    id: {
      type: DataTypes.BIGINT(20),
      autoIncrement: true,
      primaryKey: true,
    },
    name: DataTypes.STRING(50),
  }, {
    tableName: 'type_room',
    freezeTableName: true,
    timestamps: false,
    underscored: true,
  });

  return RoomType;
};
