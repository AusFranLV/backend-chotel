module.exports = (sequelize, DataTypes) => {
  const Room = sequelize.define('Room', {
    id: {
      type: DataTypes.BIGINT(20),
      autoIncrement: true,
      primaryKey: true,
    },
    typeRoomId:  {
      type: DataTypes.BIGINT(20),
      field: 'type_room_id',
      allowNull: false,
    },
    name: DataTypes.STRING(50),
    status: {
      type: DataTypes.INTEGER(1),
      defaultValue: 0,
    }
  }, {
    tableName: 'room',
    freezeTableName: true,
    timestamps: false,
    underscored: true,
  });

  return Room;
};
